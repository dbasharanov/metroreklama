namespace :ad_positions do
  desc 'Update ad positions from metroreklamas API'
  task update: :environment do
    mreklama = Mreklama.new
    types = mreklama.get_ad_position_types
    current_ids = AdPosition.all.map(&:id)
    ids = []
    types.each do |type|
      positions = mreklama.get_ad_positions type['id']
      positions.each do |pos|
        ids << pos['id']
      end
    end
    ids_for_deletion = []
    ids_for_deletion = current_ids - ids unless ids.empty?
    ids_for_deletion.each do |d|
      AdPositions.find(d).destroy!
    end

    ids.each do |id|
      position = mreklama.ad_position(id)
      ad_position = AdPosition.find_or_initialize_by(id: id)
      ad_position.image = open position['image'] unless position['image'].empty?
      position['location'] = position['location']['name']
      position['ad_type'] = position['type']['name']
      position.delete 'type'
      ad_position.update_attributes position
    end
  end
end
