class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :client
      t.integer :year
      t.string :meta_description
      t.string :meta_keywords
      t.integer :partner_id, index: true
      t.timestamps null: false
    end
    reversible do |dir|
      dir.up do
        Project.create_translation_table! client: :string,
                                          meta_description: :string,
                                          meta_keywords: :string
      end
      dir.down do
        Project.drop_translation_table!
      end
    end
  end
end
