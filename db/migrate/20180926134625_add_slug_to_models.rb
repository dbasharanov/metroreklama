class AddSlugToModels < ActiveRecord::Migration
  def change
    add_column :news, :slug, :string, unique: true
    add_column :categories, :slug, :string, unique: true
    add_column :projects, :slug, :string, unique: true
    add_column :partners, :slug, :string, unique: true
  end
end
