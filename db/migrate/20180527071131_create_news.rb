class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.string :description
      t.string :meta_description
      t.string :meta_keywords
      t.text :content
      t.timestamps null: false
    end

    reversible do |dir|
      dir.up do
        News.create_translation_table! title: :string,
                                       description: :string,
                                       meta_keywords: :string,
                                       meta_description: :string,
                                       content: :text
      end
      dir.down do
        News.drop_translation_table!
      end
    end
  end
end
