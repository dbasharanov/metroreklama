class AddTypeToProject < ActiveRecord::Migration
  def change
    add_column :projects, :ad_type_id, :integer, index: true
  end
end
