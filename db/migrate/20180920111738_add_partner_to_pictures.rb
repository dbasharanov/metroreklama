class AddPartnerToPictures < ActiveRecord::Migration
  def change
    add_reference :pictures, :partner, index: true
  end
end
