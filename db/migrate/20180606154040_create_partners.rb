class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.boolean :show_on_homepage
      t.text :content
      t.attachment :picture
      t.attachment :logo
      t.string :meta_description
      t.string :meta_keywords
      t.integer :category_id, index: true
      t.timestamps null: false
    end
    reversible do |dir|
      dir.up do
        Partner.create_translation_table! name: :string,
                                          content: :string,
                                          meta_description: :string,
                                          meta_keywords: :string
      end
      dir.down do
        Partner.drop_translation_table!
      end
    end
  end
end
