class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
      t.string :name
      t.string :position
      t.attachment :avatar
      t.timestamps null: false
    end

    reversible do |dir|
      dir.up do
        TeamMember.create_translation_table! name: :string,
                                        position: :string
      end
      dir.down do
        TeamMember.drop_translation_table!
      end
    end
  end
end
