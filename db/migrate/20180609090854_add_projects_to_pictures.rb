class AddProjectsToPictures < ActiveRecord::Migration
  def change
    add_reference :pictures, :project, index: true
  end
end
