class CreateAdPositions < ActiveRecord::Migration
  def change
    create_table :ad_positions do |t|
      t.string :name
      t.integer :number
      t.boolean :light
      t.integer :width
      t.integer :height
      t.string :latitude
      t.string :longitude
      t.string :near_objects
      t.attachment :image
      t.string :ad_type
      t.string :location
      t.string :distance
      t.string :scheme
      t.string :road_type
      t.timestamps null: false
    end
  end
end
