class AddTextToMetum < ActiveRecord::Migration
  def change
    add_column :meta, :general_purpose_text4, :text

    reversible do |dir|
      dir.up do
        Metum.add_translation_fields! general_purpose_text4: :text
      end

      dir.down do
        remove_column :meta, :general_purpose_text4
      end
    end

  end
end
