class CreateMeta < ActiveRecord::Migration
  def change
    create_table :meta do |t|
      t.string :slug
      t.string :title
      t.string :description
      t.string :keywords
      t.text :general_purpose_text1
      t.text :general_purpose_text2
      t.text :general_purpose_text3
      t.timestamps null: false
    end

    reversible do |dir|
      dir.up do
        Metum.create_translation_table! title: :string,
                                        keywords: :string,
                                        description: :string,
                                        general_purpose_text1: :text,
                                        general_purpose_text2: :text,
                                        general_purpose_text3: :text
      end
      dir.down do
        Metum.drop_translation_table!
      end
    end
    Metum.create(slug: 'home',
                 title: 'Метрореклама',
                 keywords: 'метрореклама',
                 description: 'метрореклама')
    Metum.create(slug: 'news',
                 title: 'Новини',
                 keywords: 'новини',
                 description: 'новини',
                 general_purpose_text1: 'За Външната реклама',
                 general_purpose_text2: 'Ние сме Метрореклама – компания за реклама извън дома, която ня copy')
  end
end
