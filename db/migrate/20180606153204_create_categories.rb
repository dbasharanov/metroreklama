class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :meta_description
      t.string :meta_keywords
      t.attachment :picture
      t.timestamps null: false
    end

    reversible do |dir|
      dir.up do
        Category.create_translation_table! name: :string,
                                           meta_description: :string,
                                           meta_keywords: :string
      end
      dir.down do
        Category.drop_translation_table!
      end
    end
  end
end
