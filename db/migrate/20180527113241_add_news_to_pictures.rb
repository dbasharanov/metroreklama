class AddNewsToPictures < ActiveRecord::Migration
  def change
    add_reference :pictures, :news, index: true
  end
end
