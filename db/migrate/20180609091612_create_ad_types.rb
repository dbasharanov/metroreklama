class CreateAdTypes < ActiveRecord::Migration
  def change
    create_table :ad_types do |t|
      t.string :name
      t.timestamps null: false
    end
    reversible do |dir|
      dir.up do
        AdType.create_translation_table! name: :string
      end
      dir.down do
        AdType.drop_translation_table!
      end
    end
    ["МЕТРОКУТИИ", "БИЛБОРДИ", "СИТИ ЛАЙТ", "МЕТРОБОРДОВЕ", "МЕГАБОРДИ", "МЕТРОПАНА"].each do |t|
      AdType.create(name: t)
    end
  end
end
