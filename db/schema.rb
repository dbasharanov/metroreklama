# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181020073418) do

  create_table "ad_positions", force: :cascade do |t|
    t.string   "name"
    t.integer  "number"
    t.boolean  "light"
    t.integer  "width"
    t.integer  "height"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "near_objects"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "ad_type"
    t.string   "location"
    t.string   "distance"
    t.string   "scheme"
    t.string   "road_type"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "ad_type_translations", force: :cascade do |t|
    t.integer  "ad_type_id", null: false
    t.string   "locale",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  add_index "ad_type_translations", ["ad_type_id"], name: "index_ad_type_translations_on_ad_type_id"
  add_index "ad_type_translations", ["locale"], name: "index_ad_type_translations_on_locale"

  create_table "ad_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.string   "meta_description"
    t.string   "meta_keywords"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "slug"
    t.integer  "position"
  end

  create_table "category_translations", force: :cascade do |t|
    t.integer  "category_id",      null: false
    t.string   "locale",           null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "name"
    t.string   "meta_description"
    t.string   "meta_keywords"
  end

  add_index "category_translations", ["category_id"], name: "index_category_translations_on_category_id"
  add_index "category_translations", ["locale"], name: "index_category_translations_on_locale"

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "contacts", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "email",      null: false
    t.text     "message",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "meta", force: :cascade do |t|
    t.string   "slug"
    t.string   "title"
    t.string   "description"
    t.string   "keywords"
    t.text     "general_purpose_text1"
    t.text     "general_purpose_text2"
    t.text     "general_purpose_text3"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.text     "general_purpose_text4"
  end

  create_table "metum_translations", force: :cascade do |t|
    t.integer  "metum_id",              null: false
    t.string   "locale",                null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "title"
    t.string   "keywords"
    t.string   "description"
    t.text     "general_purpose_text1"
    t.text     "general_purpose_text2"
    t.text     "general_purpose_text3"
    t.text     "general_purpose_text4"
  end

  add_index "metum_translations", ["locale"], name: "index_metum_translations_on_locale"
  add_index "metum_translations", ["metum_id"], name: "index_metum_translations_on_metum_id"

  create_table "news", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.string   "meta_description"
    t.string   "meta_keywords"
    t.text     "content"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "slug"
  end

  create_table "news_translations", force: :cascade do |t|
    t.integer  "news_id",          null: false
    t.string   "locale",           null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "title"
    t.string   "description"
    t.string   "meta_keywords"
    t.string   "meta_description"
    t.text     "content"
  end

  add_index "news_translations", ["locale"], name: "index_news_translations_on_locale"
  add_index "news_translations", ["news_id"], name: "index_news_translations_on_news_id"

  create_table "partner_translations", force: :cascade do |t|
    t.integer  "partner_id",       null: false
    t.string   "locale",           null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "name"
    t.string   "content"
    t.string   "meta_description"
    t.string   "meta_keywords"
  end

  add_index "partner_translations", ["locale"], name: "index_partner_translations_on_locale"
  add_index "partner_translations", ["partner_id"], name: "index_partner_translations_on_partner_id"

  create_table "partners", force: :cascade do |t|
    t.string   "name"
    t.boolean  "show_on_homepage"
    t.text     "content"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "meta_description"
    t.string   "meta_keywords"
    t.integer  "category_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "slug"
  end

  add_index "partners", ["category_id"], name: "index_partners_on_category_id"

  create_table "pictures", force: :cascade do |t|
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.datetime "data_updated_at"
    t.integer  "news_id"
    t.integer  "project_id"
    t.integer  "partner_id"
  end

  add_index "pictures", ["news_id"], name: "index_pictures_on_news_id"
  add_index "pictures", ["partner_id"], name: "index_pictures_on_partner_id"
  add_index "pictures", ["project_id"], name: "index_pictures_on_project_id"

  create_table "project_translations", force: :cascade do |t|
    t.integer  "project_id",       null: false
    t.string   "locale",           null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "client"
    t.string   "meta_description"
    t.string   "meta_keywords"
  end

  add_index "project_translations", ["locale"], name: "index_project_translations_on_locale"
  add_index "project_translations", ["project_id"], name: "index_project_translations_on_project_id"

  create_table "projects", force: :cascade do |t|
    t.string   "client"
    t.integer  "year"
    t.string   "meta_description"
    t.string   "meta_keywords"
    t.integer  "partner_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "ad_type_id"
    t.string   "slug"
  end

  add_index "projects", ["partner_id"], name: "index_projects_on_partner_id"

  create_table "team_member_translations", force: :cascade do |t|
    t.integer  "team_member_id", null: false
    t.string   "locale",         null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "name"
    t.string   "position"
  end

  add_index "team_member_translations", ["locale"], name: "index_team_member_translations_on_locale"
  add_index "team_member_translations", ["team_member_id"], name: "index_team_member_translations_on_team_member_id"

  create_table "team_members", force: :cascade do |t|
    t.string   "name"
    t.string   "position"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

end
