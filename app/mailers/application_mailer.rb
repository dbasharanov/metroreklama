class ApplicationMailer < ActionMailer::Base
  def contact_mail(contact_info)
    @contact_info = contact_info
    mail(to: Rails.application.secrets.notification_email, subject: 'Ново съобщение от контактна форма')
  end
end
