class ContactsController < ApplicationController

  def new
    @metum = Metum.find_by_slug('contacts')  
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)

    if verify_recaptcha(model: @contact) && @contact.save
      ApplicationMailer.contact_mail(@contact).deliver_later
      redirect_to new_contact_path, notice: t('.success')
    else
      flash.now[:error] = t('.error')
      render :new
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:name, :email, :message, :captcha, :captcha_key)
    end
end
