class NewsController < ApplicationController
  before_action :set_news, only: [:show]

  def index
    @metum = Metum.find_by_slug('news')
    @news = News.all.page(params[:page]).per(3)
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def news_params
      params.fetch(:news, {})
    end
end
