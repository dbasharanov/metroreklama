class ProjectsController < ApplicationController
  before_action :set_project, only: [:show]

  def index
  	@metum = Metum.find_by_slug('projects')

    adtype = params[:adtype]
    partner = params[:partner]

  	@projects = Project.all.page(params[:page]).per(6)
    @projects = Project.where(ad_type_id: adtype).page(params[:page]).per(6) if !adtype.to_s.empty?
    @projects = Project.where(partner_id: partner).page(params[:page]).per(6) if  !partner.to_s.empty?
    @projects = Project.where(ad_type_id: adtype, partner_id: partner).page(params[:page]).per(6) if !adtype.to_s.empty? && !partner.to_s.empty?
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.friendly.find(params[:id])
    end
end
