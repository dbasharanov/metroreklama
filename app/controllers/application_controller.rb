class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale, :init#, :authenticate
  http_basic_authenticate_with name: "metro", password: "reklama"

  def init
    @metum = Metum.first
  end

  def set_locale
    I18n.backend.reload! if Rails.env == 'development'
    if Rails.application.config.i18n.available_locales.include? params[:locale]
      I18n.locale = params[:locale]
      cookies.permanent[:locale] = params[:locale]
    else
      if cookies.key?(:locale) && Rails.application.config.i18n.available_locales.include?(cookies[:locale])
        I18n.locale = cookies[:locale]
      else
        I18n.default_locale
      end
    end
  end
end
