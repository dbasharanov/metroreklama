class MapController < ApplicationController

  def index
    @metum = Metum.find_by_slug('map')  

    @ad_positions = []
    ad_type = params[:ad_type]
    name = params[:name]
    
    positions = AdPosition.all
    positions = AdPosition.where(ad_type: ad_type).where.not(longitude: "") if !ad_type.to_s.empty?
    positions = AdPosition.where(name: name) if  !name.to_s.empty?
    positions = AdPosition.where(ad_type: ad_type, name: name) if !ad_type.to_s.empty? && !name.to_s.empty?
      
    positions.all.each do |position|
      @ad_positions << [position.name, position.latitude.to_f, position.longitude.to_f,
              position.number, position.location,
              position.ad_type, position.scheme, position.image.url]
    end

  end
end
