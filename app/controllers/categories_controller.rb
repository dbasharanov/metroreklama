class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  def index
    @metum = Metum.find_by_slug('categories')
    @categories = Category.order(position: :asc)
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.friendly.find(params[:id])
    end
end
