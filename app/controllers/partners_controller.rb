class PartnersController < ApplicationController
  before_action :set_partner, only: [:show]

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner
      @partner = Partner.friendly.find(params[:id])
    end
end
