// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require flickity.min
//= require_tree .

$(function(){ 
  $(document).foundation(); 
  toggleMenu();

  $('#ad-format-form').click(function() {
    $(this).submit();
  });

  $(document).scroll(function () { 
    y = $(this).scrollTop();
    var height = $( window ).height()/2
    if (y > 0) {  
      //$('#homepage').css("display", "none");
      $('nav#main-menu').addClass("transparent");
    }
    else {
      $('nav#main-menu').removeClass("transparent");
    }
  });
});


function toggleMenu() {
  var container = $(".dropdown-menu");
  $('.hamburger').click(function (e) { 
    $('.hamburger').toggleClass('is-active');
    $(".dropdown-menu").toggle();
  });
  $(document).keydown(function(ev) {
    if (ev.keyCode == 27) {
      container.hide();
      $('.hamburger').removeClass('is-active');
    }
  });
}
