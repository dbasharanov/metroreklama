class AdType < ActiveRecord::Base
  translates :name
  accepts_nested_attributes_for :translations, allow_destroy: true
  has_many :projects
end
