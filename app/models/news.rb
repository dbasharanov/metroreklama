class News < ActiveRecord::Base
  include OgImageable
  extend FriendlyId
  friendly_id :title, use: :slugged

  translates :title,
             :description,
             :meta_keywords,
             :meta_description,
             :content
  accepts_nested_attributes_for :translations, allow_destroy: true

  alias_attribute :name, :title
  has_many :pictures

end
