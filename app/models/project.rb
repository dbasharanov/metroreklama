class Project < ActiveRecord::Base
  extend FriendlyId
  friendly_id :client, use: :slugged

  translates :client,
             :meta_keywords,
             :meta_description
  accepts_nested_attributes_for :translations, allow_destroy: true

  belongs_to :partner
  belongs_to :ad_type
  has_many :pictures
end
