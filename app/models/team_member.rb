class TeamMember < ActiveRecord::Base
  translates :name, :position
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_attached_file :avatar,
                    styles: {
                        normal: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "350x",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        thumbnail: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "200x200#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        original: { animated:true, quality: 100, geometry: '' }
                      },
                      convert_options: {
                        default: '-quality 80 -interlace Plane -strip',
                      },
                      processors: [:thumbnail, :paperclip_optimizer]

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
end
