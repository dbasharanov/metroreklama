class AdPosition < ActiveRecord::Base

  alias_attribute :have_light, :light
  alias_attribute :lat, :latitude
  alias_attribute :long, :longitude

  scope :for_billa, -> {
    where(scheme: "Била")
  }

  has_attached_file :image,
                      styles: {
                        normal: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "640x420#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        thumbnail: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "200x200#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        original: { animated:true, quality: 100, geometry: '' }
                      },
                      convert_options: {
                        default: '-quality 80 -interlace Plane -strip',
                      },
                      processors: [:thumbnail, :paperclip_optimizer]

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/


    def self.map_stats
    res = []
    AdPosition.all.each do |position|
      res << [position.name, position.latitude.to_f, position.longitude.to_f,
              position.number, position.location,
              position.ad_type, position.scheme, position.image.url]
    end
    res
  end

end