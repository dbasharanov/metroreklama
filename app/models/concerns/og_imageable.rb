module OgImageable
  extend ActiveSupport::Concern

  included do
    def og_image
      picture_url = '/missing.jpg'
      if defined? pictures
        picture_url = pictures.first.data.url if pictures.any?
      end
      "#{Rails.application.config.base_url}#{picture_url}"
    end
  end
end
