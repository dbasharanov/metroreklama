class Metum < ActiveRecord::Base
  include OgImageable

  translates :title,
             :keywords,
             :description,
             :general_purpose_text1,
             :general_purpose_text2,
             :general_purpose_text3,
             :general_purpose_text4

  accepts_nested_attributes_for :translations, allow_destroy: true

end
