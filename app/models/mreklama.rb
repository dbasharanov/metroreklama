class Mreklama
  attr_accessor :response

  API_URL = 'http://api.metroreklama.com/api/'
  TOKEN = 'ekSd74FekqwGh92dsRgf451S3Df'

  def get_ad_position_types
    request('types')
    response['list']
  end

  def get_ad_positions(type_id)
    params = { type: type_id }
    request('resources', params)
    response['list']
  end

  def ad_position(id)
    params = { id: id }
    request('resources', params)
    response['list'].first
  end

  protected
  def parameterize (hash = {})
    hash[:token] = TOKEN
    '/' + URI.escape(hash.collect{|k,v| "#{k}/#{v}"}.join('/'))
  end

  def request (method = '', params = {})
    url = URI.parse(API_URL + method  + parameterize(params))
    self.response = JSON.parse(Net::HTTP.get(url))
  end

  def is_success?
    response.result.to_i =~ 0
  end

end
