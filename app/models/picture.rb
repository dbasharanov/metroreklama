class Picture < ActiveRecord::Base
  belongs_to :news
  belongs_to :project
  belongs_to :partner
  has_attached_file :data,
                    styles: {
                        normal: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "640x420#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        thumbnail: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "200x200#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        original: { animated:true, quality: 100, geometry: '' }
                      },
                      convert_options: {
                        default: '-quality 80 -interlace Plane -strip',
                      },
                      processors: [:thumbnail, :paperclip_optimizer]

  validates_attachment_content_type :data, content_type: /\Aimage\/.*\z/

end