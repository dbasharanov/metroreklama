class Category < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  translates :name,
             :meta_keywords,
             :meta_description
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_attached_file :picture,
                    styles: {
                        normal: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "640x360#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        thumbnail: {
                          format: 'jpg',
                          quality: 80,
                          geometry: "200x200#",
                          paperclip_optimizer: { jpegoptim:  { max_quality:  90 } }
                          },
                        original: { animated:true, quality: 100, geometry: '' }
                      },
                      convert_options: {
                        default: '-quality 80 -interlace Plane -strip',
                      },
                      processors: [:thumbnail, :paperclip_optimizer]

  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
  has_many :partners

  def path
    path =  "/categories/#{self.slug}" if self.partners.count > 1
    path =  "/partners/#{self.partners.take.slug}" if self.partners.count == 1
    return path
  end
end
