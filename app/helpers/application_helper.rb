module ApplicationHelper
  def title
    object = instance_variable_get('@'+controller_name.singularize)
    if object.kind_of? ActiveRecord::Base
      second_part = object.name
    else
      second_part = @metum.title
    end
    title = " #{Rails.application.config.site_name} | #{second_part}"
    title
  end

  def meta_data
    object = instance_variable_get('@'+controller_name.singularize)
    meta = {}

    if object.kind_of? ActiveRecord::Base
      meta[:keywords] = object.meta_keywords if object[:meta_keywords].present?
      meta[:description] = object.meta_description if object[:meta_description].present?
    end

    meta.reverse_merge!({
      keywords: @metum.keywords,
      description: @metum.description,
    }) if meta[:keywords].blank? or meta[:description].blank?

    meta[:'og:site_name'] = Rails.application.config.site_name
    meta[:'og:title'] = title
    meta[:'og:url'] = request.original_url
    meta[:'og:locale'] = I18n.locale
    meta[:'og:image'] = (object.try(:og_image)) ? object.og_image : @metum.og_image
    meta[:'og:description'] = meta[:description]
    meta
  end

  def meta_data_tags
    meta_data.map do |name, content|
      if name.to_s.include?('og:')
        tag('meta', property: name, content: content)
      else
        tag('meta', name: name, content: content)
      end
    end.join("\n")
  end
end
