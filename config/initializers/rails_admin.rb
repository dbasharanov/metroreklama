RailsAdmin.config do |config|

#  config.authenticate_with do
#    authenticate_or_request_with_http_basic do |username, password|
#      username == 'metro' && password == 'reklama'
#    end
#  end

  config.included_models = ['News', 'News::Translation',
                            'Metum', 'Metum::Translation',
                            'Category', 'Category::Translation',
                            'Partner', 'Partner::Translation',
                            'TeamMember', 'TeamMember::Translation',
                            'Project', 'Project::Translation',
                            'AdType', 'AdType::Translation',
                            'Picture',
                            'Contact']

  config.model News do
    field :translations, :globalize_tabs
    fields :slug, :pictures
  end
  config.model News::Translation do
    field :description, :ck_editor
    field :content, :ck_editor
    include_fields :title, :description, :content, :meta_description, :meta_keywords
  end
  
  config.model Metum do
    field :translations, :globalize_tabs
    field :slug
  end
  config.model Metum::Translation do
    configure :locale, :hidden do
      help ''
    end
    visible true
    configure :general_purpose_text1, :ck_editor
    configure :general_purpose_text2, :ck_editor
    configure :general_purpose_text3, :ck_editor
    configure :general_purpose_text4, :ck_editor
    include_fields :locale, :title, :description, :keywords, :general_purpose_text1, :general_purpose_text2, :general_purpose_text3, :general_purpose_text4
  end
  
  config.model Category do
    field :translations, :globalize_tabs
    fields :slug, :position, :picture, :partners
  end
  config.model Category::Translation do
    configure :locale, :hidden do
      help ''
    end
    visible true
    include_fields :locale, :name, :meta_description, :meta_keywords
  end
  
  config.model Partner do
    field :translations, :globalize_tabs
    fields :slug, :category, :logo, :picture, :pictures, :projects, :show_on_homepage
  end
  config.model Partner::Translation do
    configure :locale, :hidden do
      help ''
    end
    visible true
    configure :content, :ck_editor
    include_fields :locale, :name, :content, :meta_description, :meta_keywords
  end

  config.model TeamMember do
    field :translations, :globalize_tabs
    field :avatar
  end
  config.model TeamMember::Translation do
    configure :locale, :hidden do
      help ''
    end
    visible true
    include_fields :locale, :name, :position
  end

  config.model Project do
    field :translations, :globalize_tabs
    fields :slug, :year, :partner, :ad_type
  end
  config.model Project::Translation do
    configure :locale, :hidden do
      help ''
    end
    visible true
    include_fields :locale, :client, :meta_description, :meta_keywords
  end

  config.model AdType do
    field :translations, :globalize_tabs
    field :projects
  end
  config.model AdType::Translation do
    configure :locale, :hidden do
      help ''
    end
    visible true
    include_fields :locale, :name
  end



  
  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    export
    bulk_delete
    show
    edit
    delete do
      except %w(Metum)
    end
    new
  end
end
