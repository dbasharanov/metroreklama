role :app, %w(164.138.220.184)
role :web, %w(164.138.220.184)
role :db,  %w(164.138.220.184)
set :deploy_to, '/home/metroreklama/metroreklama'

server '164.138.220.184', roles: %w(web app), ssh_options: { user: 'metroreklama', port: 22 } # , my_property: :my_value

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute '/home/metroreklama/rc.d/rc.unicorn-metroreklama restart'
    end
  end
end
