# config valid only for current version of Capistrano
lock "3.8.1"

set :application, "metroreklama"
set :repo_url, "git@bitbucket.org:vkdimitrov/metroreklama.git"
set :branch, ENV['branch'] || 'master'

set :linked_files, fetch(:linked_files, []).push('config/unicorn.rb', 'db/production.sqlite3')
set :linked_dirs, %w(log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/ckeditor_assets)

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do
  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end